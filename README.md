# 実行

VSCode の場合

C-S-d

```json
{
    "version": "0.2.0",
    "configurations": [
        {
            "type": "java",
            "name": "Spring Boot App",
            "request": "launch",
            "mainClass": "com.example.demo.SpringBootSample1Application"
        }
    ]
}
```

# 設定

ポートの変更: application.properties


# VSCode の設定

- スペースでインデント
- インデント幅

```json
    "[java]": {
        "editor.tabSize": 4,
        "editor.insertSpaces": true,
        "editor.suggest.snippetsPreventQuickSuggestions": false
    },
    "[html]": {
        "editor.tabSize": 2,
        "editor.insertSpaces": true,
        "editor.suggest.insertMode": "replace"
    }
```

# メモ

Rails のルーティング - Railsガイド  
https://railsguides.jp/routing.html
