package com.example.demo.dbm.tdef;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

@SpringBootTest
@TestExecutionListeners({
    DependencyInjectionTestExecutionListener.class,
    DbUnitTestExecutionListener.class
})
class TdefServiceTest {

    private final TdefService sut;

    @Autowired
    public TdefServiceTest(TdefService tdefService) {
        this.sut = tdefService;
    }

    @Test
    @DatabaseSetup("/db/data.xml")
    void test() {
        // 実行
        List<Tdef> tdefs = sut.getTdefs();

        assertEquals(2, tdefs.size());

        tdefs.sort((a, b) -> a.getId() - b.getId());

        {
            Tdef tdef = tdefs.get(0);
            assertEquals(1, tdef.getId());
            assertEquals("t1", tdef.getPname());
        }
        {
            Tdef tdef = tdefs.get(1);
            assertEquals(2, tdef.getId());
            assertEquals("t2", tdef.getPname());
        }
    }

}
