package com.example.demo.top;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.Arrays;
import java.util.List;

import com.example.demo.util.Utils;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@SpringBootTest
class SampleApiControllerTest {

    private final MockMvc mockMvc;

    @Autowired
    SampleApiControllerTest() {
        SampleApiController controller = new SampleApiController();

        this.mockMvc = MockMvcBuilders
            .standaloneSetup(controller)
            .build();
    }

    @Test
    void test_rawJson() throws Exception {
        MvcResult result =
            this.mockMvc.perform(
                MockMvcRequestBuilders.get("/sample-api/raw-json")
            )
            .andDo(print())
            .andExpect(status().isOk())
            .andReturn()
        ;

        String expected = Utils.mkjson(
            Arrays.asList(
                "{",
                "  ^a^: 456",
                "}"
            ),
            "^"
        );

        assertEquals(
            expected,
            result.getResponse().getContentAsString()
        );
    }

    @Test
    void test_responseEntity_string() throws Exception {
        MvcResult result =
            this.mockMvc.perform(
                MockMvcRequestBuilders.get("/sample-api/responseEntity-string")
            )
            .andDo(print())
            .andExpect(status().isOk())
            .andReturn()
        ;

        String expected = Utils.mkjson(
            Arrays.asList(
                "{",
                "  ^a^: 456",
                "}"
            ),
            "^"
        );

        assertEquals(
            expected,
            result.getResponse().getContentAsString()
        );
    }

}
