package com.example.demo.util;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

public class Utils {

    public static String lines(List<String> lines) {
        return lines.stream().collect(Collectors.joining("\n"));
    }

    public static String mkjson(List<String> lineStrs, String quoteChar) {
        return StringUtils.replace(
            lines(lineStrs), quoteChar, "\"");
    }

}
