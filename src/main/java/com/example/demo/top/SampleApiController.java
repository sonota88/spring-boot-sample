package com.example.demo.top;

import java.util.Arrays;
import java.util.List;

import com.example.demo.util.Utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sample-api")
public class SampleApiController {

    @RequestMapping(
        value = "/raw-json",
        method = RequestMethod.GET,
        produces = "application/json"
    )
    public String rawJson() {
        String json = Utils.mkjson(
            Arrays.asList(
                "{",
                "  ^a^: 456",
                "}"
            ),
            "^"
        );

        return json;
    }

    @RequestMapping(
        value = "/responseEntity-string",
        method = RequestMethod.GET,
        produces = "application/json"
    )
    public ResponseEntity<String> responseEntity_string() {
        String json = Utils.mkjson(
            Arrays.asList(
                "{",
                "  ^a^: 456",
                "}"
            ),
            "^"
        );

        return new ResponseEntity<String>(json, HttpStatus.OK);
    }

}
