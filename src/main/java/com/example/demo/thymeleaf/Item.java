package com.example.demo.thymeleaf;

public class Item {
    private int id;
    private String name;

    public Item (int id, String name) {
        this.setId(id);
        this.setName(name);
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
