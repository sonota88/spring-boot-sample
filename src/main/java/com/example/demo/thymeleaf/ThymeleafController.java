package com.example.demo.thymeleaf;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/thymeleaf")
public class ThymeleafController {

    @GetMapping("")
    public String index(Model model) {
        model.addAttribute("foo", "FOO");
        model.addAttribute("items", getItems());
        model.addAttribute("now", LocalDateTime.now());
        return "thymeleaf";
    }

    private List<Item> getItems() {
        return Arrays.asList(
            new Item(1, "foo"),
            new Item(2, "bar")
        );
    }

}
