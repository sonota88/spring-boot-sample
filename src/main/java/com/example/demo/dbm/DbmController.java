package com.example.demo.dbm;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DbmController {

    @GetMapping("/dbm")
    public String top() {
        return "dbm/top";
    }

}
