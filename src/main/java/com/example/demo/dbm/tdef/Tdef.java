package com.example.demo.dbm.tdef;

import org.springframework.util.Assert;

public class Tdef {

    /**
     * insert 時に一時的に null になる
     */
    private Integer id;

    private String pname;

    public Tdef(
        Integer id,
        String pname
    ) {
        this.id = id;

        Assert.notNull(pname, "not null");
        this.pname = pname;
    }

    public static Tdef from(
        int id,
        Tdef tdef
    ) {
        return new Tdef(id, tdef.getPname());
    }

    public int getId() {
        return this.id;
    }

    public String getPname() {
        return this.pname;
    }

}
