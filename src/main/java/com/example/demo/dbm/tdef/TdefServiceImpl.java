package com.example.demo.dbm.tdef;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;

@Service
class TdefServiceImpl implements TdefService {

    private final JdbcTemplate jdbcTemplate;

    TdefServiceImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Tdef> getTdefs() {
        String sql = "select id, pname from tdefs";

        List<Tdef> tdefs = jdbcTemplate.query(
            sql,
            new TdefMapper()
        );

        return tdefs;
    }

    @Override
    public Tdef add(Tdef tdef) {
        SqlParameterSource param = new BeanPropertySqlParameterSource(tdef);
        SimpleJdbcInsert insert =
            new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("tdefs")
                .usingGeneratedKeyColumns("id")
        ;
        Number id = insert.executeAndReturnKey(param);
        return Tdef.from(id.intValue(), tdef);
    }

}
