package com.example.demo.dbm.tdef;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class TdefMapper implements RowMapper<Tdef> {

    @Override
    public Tdef mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Tdef(
            rs.getInt("id"),
            rs.getString("pname")
        );
    }

}
