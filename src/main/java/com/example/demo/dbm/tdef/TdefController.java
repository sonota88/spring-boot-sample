package com.example.demo.dbm.tdef;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
@Controller
@RequestMapping("/dbm/tdef")
public class TdefController {

    TdefService tdefService;

    public TdefController(TdefService tdefService) {
        this.tdefService = tdefService;
    }

    @GetMapping("/")
    public String index(Model model) {
        List<Tdef> tdefs = tdefService.getTdefs();

        model.addAttribute("tdefs", tdefs);

        return "dbm/tdef/index";
    }

    @GetMapping("/new")
    public String newForm() {
        return "dbm/tdef/new";
    }

    @PostMapping("/")
    public String create(
        @RequestParam String pname,
        Model model
    ) {
        Tdef tdef = new Tdef(null, pname);
        tdefService.add(tdef);
        return index(model);
    }

    @GetMapping("/{id}/edit")
    public String editForm(
        @PathVariable int id,
        Model model
    ) {
        Tdef tdef = new Tdef(1, "tdef1");

        model.addAttribute("tdef", tdef);

        return "dbm/tdef/edit";
    }

    @PutMapping("/{id}")
    public String update(
        @PathVariable int id,
        @ModelAttribute Tdef tdef,
        Model model
    ) {
        model.addAttribute("tdef", tdef);

        return "dbm/tdef/edit";
    }

}
