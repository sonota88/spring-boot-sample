package com.example.demo.dbm.tdef;

import java.util.List;

interface TdefService {

    public List<Tdef> getTdefs();

    public Tdef add(Tdef tdef);

}
